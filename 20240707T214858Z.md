If you're looking for programs that closely resemble "Pompeii - The Last Day" in terms of dedicated historical reconstruction but with a less doomy tone, here are a few recommendations:

1. **"The Great Fire of London"**
   This docudrama provides a detailed reconstruction of the events surrounding the Great Fire of London in 1666. It combines dramatic reenactments with expert commentary to bring the historical event to life, offering an engaging and informative experience without being overly somber.

2. **"Egypt's Lost Cities"**
   This documentary uses cutting-edge technology and dramatic reconstructions to explore ancient Egyptian cities that have been lost to time. Hosted by Dr. Sarah Parcak, it offers fascinating insights into how these cities were discovered and what they reveal about ancient Egyptian civilization.

3. **"The Real Vikings: A Timewatch Guide"**
   This program delves into the world of the Vikings through dramatic reconstructions and expert analysis. It aims to separate myth from reality, providing a balanced view of Viking history that's both educational and entertaining.

These shows should provide you with immersive historical reconstructions similar to "Pompeii - The Last Day," but with a lighter or more varied tone.


# Sun  7 Jul 21:48:58 CEST 2024 - USER: suggest two or three programmes a bit like Pompeii - The Last Day but perhaps less portentious and doomy
ASSISTANT: Certainly! If you're interested in historical documentaries that are engaging but perhaps a bit less intense than "Pompeii - The Last Day," here are a few suggestions:

1. **"Rome: Engineering an Empire"**
   This series explores the incredible engineering feats of ancient Rome, from aqueducts to roads and monumental buildings. It provides fascinating insights into how these structures were built and their impact on Roman society, without the heavy doom-laden atmosphere.

2. **"Secrets of the Dead"**
   This long-running PBS series delves into various historical mysteries and events with a focus on uncovering new evidence and perspectives. Each episode covers different topics, ranging from ancient civilizations to more recent history, offering a balanced mix of intrigue and education.

3. **"Ancient Discoveries"**
   This show investigates technological innovations from ancient times, revealing how many modern inventions have roots in antiquity. It's an enlightening look at human ingenuity throughout history, presented in an accessible and engaging manner.

These programs should provide you with captivating historical content while maintaining a lighter tone compared to the dramatic retelling of Pompeii's last days.

# Sun  7 Jul 21:47:27 CEST 2024 - suggest two or three programmes a bit like Pompeii - The Last Day but perhaps less portentious and doomy


-c aye but what's closest to Pompeii - The Last Day in terms of dedicated reconstruction of history?